#include "individual.h"

individual::individual()
{
   name = "";
   yearOfBirth = "";
   yearOfDeath = "";
   sex ="";
}

individual::individual(const string& n, const string& yb, const string& yd, const string& s, int newid, string IP)
{
    name = n;
    yearOfBirth = yb;
    yearOfDeath = yd;
    sex = s;
    id = newid;
    imagePath = IP;
}
ostream& operator<<(ostream& out, individual ind)
{
    out << setw(30) << left << ind.name << setw(20) << left << ind.yearOfBirth << setw(20) << left << ind.yearOfDeath << setw(5)<< left << ind.sex << endl;
    return out;
}
string individual::getName()
{
    return name;
}
string individual::getYB()
{
    return yearOfBirth;
}
string individual::getYD()
{
    return yearOfDeath;
}
string individual::getSex()
{
    return sex;
}

int individual::getID()
{
    return id;
}

string individual::getImagePath()
{
    return imagePath;
}
