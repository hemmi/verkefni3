#ifndef VIEWCONNECTIONS_H
#define VIEWCONNECTIONS_H

#include <QDialog>
#include <string>
#include "computer.h"
#include "individual.h"
#include "service.h"


namespace Ui {
class viewConnections;
}

class viewConnections : public QDialog
{
    Q_OBJECT

public:
    explicit viewConnections(QWidget *parent = 0);
    ~viewConnections();

    void getIdFromMain(int id, string currentCategory);
    void populateTableSelCom(Computer selectedCom, std::list<individual> connectedScis);
    void populateTableSelSci(individual selectedSci, std::list<Computer> connectedComs);
private:
    Ui::viewConnections *ui;
    service serv;
    int forwardedID;
    string forwardedCategory;
    individual selectedSci;
    Computer selectedCom;
    std::list<individual> connectedScis;
    std::list<Computer> connectedComs;
};

#endif // VIEWCONNECTIONS_H
