import QtQuick 1.1

Rectangle {
    width: 50
    height: 50
    radius: 0
    border.width: 0
    opacity: 1

    Image {
        id: image1
        x: -113
        y: -58
        width: 50
        height: 50
        scale: 1
        rotation: 0
        z: 0
        source: "10868164_10203841043839169_138394813353579835_n.jpg"
    }
}
