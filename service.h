#ifndef SERVICE_H
#define SERVICE_H
#include "individual.h"
#include "computerrepository.h"
#include "individualrepository.h"
#include "baserepository.h"
#include <cctype>
#include <ctype.h>
#include <list>
#include <algorithm>
#include <string>
#include "sciID.h"

using namespace std;

class service
{
public:
    service();
    void remove(int id, string currentCategory);

    //functions pertaining to the Computer class
    std::list<Computer> search(string searchterm);
    string assessOrder(string choice);
    std::list<Computer> sortC(string order, string ascOrDesc);
    void addCom(const string &name, const string &type, const string &built, const string &ybuilt, string &imagePath);
    std::list<Computer> listCom();
    int getRowIDCom();
    std::list<sciID> getComIDlist();
    void connectCom(int rowID, int idToConnect);
    Computer getConnectedComputer(int id);
    std::list<Computer> ConnectedComputers(int id);

    //functions pertaining to the individual class
    list<individual> searchSci(string searchterm);
    string assessOrderS(string choice);
    std::list<individual> sortS(string order, string ascOrDesc);
    void addSci(const string &name, const string &yearb, const string &yeard, const string &sex, string imagePath);
    std::list<individual> listSci();
    int getRowIDSci();
    std::list<sciID> getSciIDlist();
    void connectSci(int rowID, int connectID);
    individual getConnectedScientist(int id);
    std::list<individual> ConnectedScientists(int id);

private:
  computerRepository comRepo;
  individualRepository indRepo;
};

#endif // SERVICE_H
