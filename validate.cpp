#include "validate.h"
#include <QDebug>

validate::validate()
{
}
void validate::validateString(string &name) //validate the input to the name field.
{
    if(islower(name[0]))
        toupper(name[0]);
}

bool validate::validateDeath(string yearOfDeath, string yearOfBirth)//validate the input to the year fields.
{
    int newYearOfBirth = atoi(yearOfBirth.c_str());
    int newYearOfDeath = atoi(yearOfDeath.c_str());
    return(newYearOfDeath > newYearOfBirth && newYearOfDeath<=2014);
}
bool validate::validateBirth(string year) //validate the input to the year fields.
{
    int newYear = atoi(year.c_str());
    return(newYear>1200 && newYear<=2014);
}

bool validate::validateSex(string &sex)//check if input is valid, if valid convert to uppercase.
{
    if(sex=="m")
    {
        sex="M";
        return true;
    }
    if(sex=="f")
    {
        sex="F";
        return true;
    }

    return false;
}
bool validate::validateYesOrNo(string input)
{
    return(input=="yes"||input=="no");
}

bool validate::validateID(std::list<sciID> list, int id){//check if input id is in list of available options.


    stringstream ss;
    ss << id;
    string str = ss.str();

    for(std::list<sciID>::iterator iter = list.begin(); iter != list.end(); iter++)
    {
        if(iter->getID() == str)
        {
            return true;
        }
    }

return false;
}

bool validate::validateYearbuilt(int year)
{
    return(year>10 && year<2014);
}
