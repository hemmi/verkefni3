#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H
#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

class individual
{
public:
    individual();
    individual(const string& n, const string& yb, const string& yd, const string& s, int newid, string IP);
    friend ostream& operator <<(ostream& out, individual ind);

    string getName();
    string getYB();
    string getYD();
    string getSex();
    int getID();
    string getImagePath();

private:
    string name;
    string yearOfBirth;
    string yearOfDeath;
    string sex;
    int id;
    string imagePath;
};

#endif // INDIVIDUAL_H
