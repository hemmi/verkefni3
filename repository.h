#ifndef REPOSITORY_H
#define REPOSITORY_H
#include <QtSql>
#include "individual.h"
#include "sciID.h"
#include "computer.h"
#include <list>
#include <vector>
#include <fstream>

using namespace std;

class repository
{
public:
    repository();
    ~repository();

    //functions pertaining to the Computer class
    void addCom(Computer com);
    std::list<Computer> getClist();
    std::list<Computer> sortC(string choice, string ascOrDesc);
    std::list<Computer> find(string searchstr);
    int getRowIDCom();
    Computer getConnectedComputer(int id);
    std::list<sciID> computersID();
    void connectCom(int id, vector<int> idlist);
    std::list<Computer> ConnectedComputers(int id);

    //functions pertaining to the individual class
    void addSci(individual ind);
    std::list<individual> getSlist();
    std::list<individual> sortS(string choice, string ascOrDesc);
    std::list<individual> findSci(string searchstr);
    int getRowIDSci();
    individual getConnectedScientist(int id);
    std::list<sciID> scientistsID();
    void connectSci(int id, vector<int> idlist);
    std::list<individual> ConnectedScientists(int id);

    void remove(int id, string currentCategory);
private:
   list<individual> slist;
   list<Computer> clist;
   list<sciID> IDlist;
   QSqlDatabase db;

};

#endif // REPOSITORY_H
