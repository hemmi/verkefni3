#include "individualrepository.h"

individualRepository::individualRepository()
{
    createConnection();
}

individualRepository::~individualRepository()
{
}
void individualRepository::addSci(individual ind)
{
    string name = ind.getName();
    string yearb = ind.getYB();
    string yeard = ind.getYD();
    string sex = ind.getSex();
    string imagePath = ind.getImagePath();

    QSqlQuery query(db); //add the new entry to the database.

    query.prepare("INSERT INTO Scientists (Name, YoB, YoD, Sex, ImagePath)"
                  "VALUES(:name, :yearb, :yeard, :sex, :imgpath)");

    query.bindValue(":name", QString::fromStdString(name));
    query.bindValue(":yearb", QString::fromStdString(yearb));
    query.bindValue(":yeard", QString::fromStdString(yeard));
    query.bindValue(":sex", QString::fromStdString(sex));
    query.bindValue(":imgpath", QString::fromStdString(imagePath));

    query.exec();
}
std::list<individual> individualRepository::getSlist()
{
   return slist;
}
std::list<individual> individualRepository::sortS(string choice, string ascOrDesc)
{
    slist.clear();
    individual ind;
    string name;
    string yearB, yearD;
    string sex;
    int id;
    string imagePath;

    //Retrieve scientists

    QSqlQuery query(db);

    query.prepare(QString("SELECT Name, YoB, YoD, Sex, ID, ImagePath FROM Scientists WHERE Deleted=0 ORDER BY %1 %2").arg(QString::fromStdString(choice)).arg(QString::fromStdString(ascOrDesc))); //Using agruments in QString Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite

    query.exec();

    while(query.next())
    {
        name = query.value("Name").toString().toStdString();
        yearB = query.value("YoB").toString().toStdString();
        yearD = query.value("YoD").toString().toStdString();
        sex = query.value("Sex").toString().toStdString();
        id = query.value("ID").toInt();
        imagePath = query.value("ImagePath").toString().toStdString();

        // Load all records into memoroy

        ind = individual(name, yearB, yearD, sex, id, imagePath);
        slist.push_back(ind);
    }
    return slist;
}

std::list<individual> individualRepository::findSci(string searchstr)
{
    slist.clear();

    individual i;
    string name;
    string yearB, yearD;
    string sex;
    int id;
    string imagePath;

    QSqlQuery query(db);

    query.prepare(QString("SELECT Name, YoB, YoD, Sex , ID, ImagePath FROM Scientists WHERE Name || YoB ||YoD || Sex LIKE '%%1%' AND Deleted=0 ORDER BY ID").arg(QString::fromStdString(searchstr))); //Using aguments in QString Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite

    query.exec();

    while(query.next())
    {
       name = query.value("Name").toString().toStdString();
       yearB = query.value("YoB").toString().toStdString();
       yearD = query.value("YoD").toString().toStdString();
       sex = query.value("Sex").toString().toStdString();
       id = query.value("ID").toInt();
       imagePath = query.value("ImagePath").toString().toStdString();
       // Load all records matching the search string into memoroy

       i = individual(name, yearB, yearD, sex, id, imagePath);
       slist.push_back(i);
    }
    return slist;
}

int individualRepository::getRowIDSci()
{

    QSqlQuery query(db);
    int rowid=0;

    query.prepare("SELECT MAX(ID) FROM Scientists WHERE Deleted=0");
    query.exec();

    while(query.next())
    {
        rowid = query.value("MAX(ID)").toInt();
    }

    return rowid;
}
individual individualRepository::getConnectedScientist(int id)
{
    individual i;

    string name;
    string yearB, yearD;
    string sex;
    int newid;
    string imagePath;

    QSqlQuery query(db);

    query.prepare(QString("SELECT Name, YoB, YoD, Sex, ID, ImagePath FROM Scientists WHERE ID is %1").arg(QString::number(id))); //Using aguments in QString Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite
    query.exec();

    while(query.next())
    {
       name = query.value("Name").toString().toStdString();
       yearB = query.value("YoB").toString().toStdString();
       yearD = query.value("YoD").toString().toStdString();
       sex = query.value("Sex").toString().toStdString();
       newid = query.value("ID").toInt();
       imagePath = query.value("ImagePath").toString().toStdString();

       i = individual(name, yearB, yearD, sex, newid, imagePath);
    }
    return i;
}

std::list<sciID> individualRepository::scientistsID()
{
    IDlist.clear();

    sciID i;

    string id,name;

    QSqlQuery query(db);

    query.prepare(QString("SELECT ID, Name FROM Scientists ORDER BY ID"));

    query.exec();

    while(query.next())
    {
        id = query.value("ID").toString().toStdString();
        name = query.value("Name").toString().toStdString();

        i = sciID(id,name);
        IDlist.push_back(i);
    }

    return IDlist;
}

void individualRepository::connectSci(int rowID, int connectedID)
{
    QSqlQuery query(db); //add the new entry to the database.

    query.prepare("INSERT INTO Connection (C_Id, S_Id)"
                      "VALUES(:cid, :sid)");
    query.bindValue(":cid", QString::number(connectedID));
    query.bindValue(":sid", QString::number(rowID));
    query.exec();
}

std::list<individual> individualRepository::ConnectedScientists(int id)
{
    individual ind;
    vector<int> tempList;
    slist.clear();

    int s_id;

    QSqlQuery query(db);

    query.prepare(QString("SELECT S_Id FROM Connection WHERE c_Id is %1").arg(QString::number(id))); //Using arguments in QString: Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite
    query.exec();

    while(query.next())
    {
        s_id = query.value("S_Id").toInt();
        tempList.push_back(s_id);
    }

    for(unsigned int i = 0; i < tempList.size(); i++)
    {
        ind =  getConnectedScientist(tempList[i]);
       slist.push_back(ind);
    }
    return slist;
}
void individualRepository::remove(int id)
{
    QSqlQuery query(db);

    query.prepare(QString("UPDATE Scientists SET Deleted=1 WHERE ID is %1").arg(QString::number(id))); //Using arguments in QString: Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite
    query.exec();
}
