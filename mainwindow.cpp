#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "addform.h"
#include <QDebug>
#include "viewconnections.h"
#include "addform.h"
#include "viewimage.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    getAllScientists();
    getAllComputers();
    ui->listEntries->verticalHeader()->setVisible(false);
    sortBy = "ID";
    ascOrDesc ="ASC";
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::getAllScientists()
{
    sciList.clear();
    sciList = serv.sortS("ID",ascOrDesc);
}
void MainWindow::getAllComputers()
{
    comList.clear();
    comList = serv.sortC("ID", ascOrDesc);
}
void MainWindow::displayAllScientists()//Display all the scientists in the database.
{
    int i = 0;
    ui->listEntries->horizontalHeaderItem(0)->setText("ID");
    ui->listEntries->horizontalHeaderItem(1)->setText("Name");
    ui->listEntries->horizontalHeaderItem(2)->setText("Year of Birth");
    ui->listEntries->horizontalHeaderItem(3)->setText("Year of Death");
    ui->listEntries->horizontalHeaderItem(4)->setText("Sex");

    ui->listEntries->clearContents();

    while (ui->listEntries->rowCount() > 0)
    {
        ui->listEntries->removeRow(0);
    }

    ui->listEntries->setColumnWidth(0,40);
    ui->listEntries->setColumnWidth(1,260);
    ui->listEntries->setColumnWidth(2,130);
    ui->listEntries->setColumnWidth(3,130);
    ui->listEntries->setColumnWidth(4,60);

    for(std::list<individual>::iterator iter = sciList.begin(); iter != sciList.end(); iter++, i++)
    {
        QTableWidgetItem *item0 = new QTableWidgetItem(QString::number(iter->getID()));
        QTableWidgetItem *item1 = new QTableWidgetItem(QString::fromStdString(iter->getName()));
        QTableWidgetItem *item2 = new QTableWidgetItem(QString::fromStdString(iter->getYB()));
        QTableWidgetItem *item3 = new QTableWidgetItem(QString::fromStdString(iter->getYD()));
        QTableWidgetItem *item4 = new QTableWidgetItem(QString::fromStdString(iter->getSex()));

        ui->listEntries->insertRow(i);
        ui->listEntries->setItem(i,0,item0);
        ui->listEntries->setItem(i,1,item1);
        ui->listEntries->setItem(i,2,item2);
        ui->listEntries->setItem(i,3,item3);
        ui->listEntries->setItem(i,4,item4);
    }
}
void MainWindow::displayAllComputers()//Display all the computers in the database.
{
    int i = 0;
    ui->listEntries->setHorizontalHeaderItem(0, new QTableWidgetItem("ID"));
    ui->listEntries->setHorizontalHeaderItem(1, new QTableWidgetItem("Name"));
    ui->listEntries->setHorizontalHeaderItem(2, new QTableWidgetItem("Type"));
    ui->listEntries->setHorizontalHeaderItem(3, new QTableWidgetItem("Built?"));
    ui->listEntries->setHorizontalHeaderItem(4, new QTableWidgetItem("Year Built"));

    ui->listEntries->clearContents();

    while (ui->listEntries->rowCount() > 0)
    {
        ui->listEntries->removeRow(0);
    }

    ui->listEntries->setColumnWidth(0,50);
    ui->listEntries->setColumnWidth(1,225);
    ui->listEntries->setColumnWidth(2,210);
    ui->listEntries->setColumnWidth(3,60);
    ui->listEntries->setColumnWidth(4,100);

    for(std::list<Computer>::iterator iter = comList.begin(); iter != comList.end(); iter++, i++)
    {
        QTableWidgetItem *item0 = new QTableWidgetItem(QString::number(iter->getID()));
        QTableWidgetItem *item1 = new QTableWidgetItem(QString::fromStdString(iter->getName()));
        QTableWidgetItem *item2 = new QTableWidgetItem(QString::fromStdString(iter->getType()));
        QTableWidgetItem *item3 = new QTableWidgetItem(QString::fromStdString(iter->getBuilt()));
        QTableWidgetItem *item4 = new QTableWidgetItem(QString::fromStdString(iter->getYB()));

        ui->listEntries->insertRow(i);
        ui->listEntries->setItem(i,0,item0);
        ui->listEntries->setItem(i,1,item1);
        ui->listEntries->setItem(i,2,item2);
        ui->listEntries->setItem(i,3,item3);
        ui->listEntries->setItem(i,4,item4);
    }
}
void MainWindow::on_categoryBox_activated(int index) //This function controls which category is being processed (scientists or computers)
{
    if(index==2){
        displayAllScientists();
        currentCategory = "Scientists";

        ui->sortBox->clear();
        ui->toolButton_sci->setEnabled(true);
        ui->toolButton_com->setDisabled(true);

        ui->sortBox->addItem("ID",1);
        ui->sortBox->addItem("Name",1);
        ui->sortBox->addItem("Year of Birth",1);
        ui->sortBox->addItem("Year of Death",1);
        ui->sortBox->addItem("Sex",1);
        disableButtons(); //gray out buttons that require a cell in the table to be selected.
    }
    if(index==1){
        displayAllComputers();
        currentCategory = "Computers";

        ui->sortBox->clear();
        ui->toolButton_com->setEnabled(true);
        ui->toolButton_sci->setDisabled(true);

        ui->sortBox->addItem("ID",1);
        ui->sortBox->addItem("Name",1);

        ui->sortBox->addItem("Type",1);
        ui->sortBox->addItem("Built?",1);
        ui->sortBox->addItem("Year Built",1);
        disableButtons(); //gray out buttons that require a cell in the table to be selected.
    }
}

void MainWindow::on_searchButton_clicked()//search for string input into search field.
{
 string searchstr = ui->search_input->text().toUtf8().constData();  //Convert qstring to std::string. Vaibhav Desai: https://stackoverflow.com/questions/4214369/how-to-convert-qstring-to-stdstring

    if (currentCategory == "Scientists")
    {
        sciList.clear();
        sciList = serv.searchSci(searchstr);
        displayAllScientists();
        disableButtons();
    }
    if (currentCategory == "Computers")
    {
        sciList.clear();
        comList = serv.search(searchstr);
        displayAllComputers();
        disableButtons();
    }
}

void MainWindow::on_resetButton_clicked() //reset the database after a search has been performed.
{
    sciList.clear();
    sciList = serv.sortS("ID",ascOrDesc);

    comList.clear();
    comList = serv.sortC("ID", ascOrDesc);

    if (currentCategory == "Computers")
    {
        displayAllComputers();
        disableButtons();
    }
    if (currentCategory == "Scientists")
    {
        displayAllScientists();
        disableButtons();
    }
    ui->search_input->clear();
}

void MainWindow::on_sortBox_activated(int index)//The funcition for the order by dropdown menu.
{
    if(currentCategory=="Scientists"){
        if (index==0)
        {
            sortBy="ID";
        }
        if (index==1)
        {
            sortBy="Name";
        }
        if (index==2)
        {
            sortBy="YoB";
        }
        if (index==3)
        {
            sortBy="YoD";
        }
        if (index==4)
        {
            sortBy="Sex";
        }
    sciList = serv.sortS(sortBy, ascOrDesc);
    displayAllScientists();
    disableButtons();
    }
    if(currentCategory=="Computers")
    {
        if (index==0)
        {
            sortBy="ID";
        }
        if (index==1)
        {
            sortBy="Name";
        }
        if (index==2)
        {
            sortBy="Ctype";
        }
        if (index==3)
        {
            sortBy="Built";
        }
        if (index==4)
        {
            sortBy="Ybuilt";
        }
    comList = serv.sortC(sortBy, ascOrDesc);
    displayAllComputers();
    disableButtons();
    }
}

void MainWindow::on_ascRadioButton_clicked()
{
    ascOrDesc="ASC";
    on_sortBox_activated(ui->sortBox->currentIndex()); //call the function where the dropdown menu is clicked to prompt an immediate change in the way the table is ordered.
    disableButtons();
}

void MainWindow::on_descRadioButton_clicked()
{
    ascOrDesc="DESC";
    on_sortBox_activated(ui->sortBox->currentIndex()); //call the function where the dropdown menu is clicked to prompt an immediate change in the way the table is ordered.
    disableButtons();
}

void MainWindow::on_listEntries_itemSelectionChanged() //enable the grayed out buttons when something in the table is selected.
{
    ui->connectionsButton->setEnabled(true);
    ui->removeButton->setEnabled(true);
    ui->viewImage->setEnabled(true);
}

void MainWindow::on_connectionsButton_clicked() //view connections relating to selected entry in table.
{
    viewConnections vcon;
    vcon.setModal(true);

    QString idString = ui->listEntries->item(ui->listEntries->currentRow(),0)->text();
    int id = idString.toInt();
    vcon.getIdFromMain(id, currentCategory);
    vcon.exec();
    disableButtons();
}
void MainWindow::disableButtons() //gray out buttons that require a cell in the table to be selected.
{
    ui->connectionsButton->setEnabled(false);
    ui->removeButton->setEnabled(false);
    ui->viewImage->setEnabled(false);
}

void MainWindow::on_search_input_cursorPositionChanged()//make sure the buttons that require a selection in the table always stay disabled if nothing is selected in the table.
{
    disableButtons();
}

void MainWindow::on_removeButton_clicked() //delete an entry in the database and retrieve all entries from the database again.
{
    QString idString = ui->listEntries->item(ui->listEntries->currentRow(),0)->text();
    int id = idString.toInt();

    serv.remove(id, currentCategory);
    if(currentCategory=="Scientists")
    {
        sciList = serv.sortS(sortBy, ascOrDesc);
        displayAllScientists();
    }
    if(currentCategory=="Computers")
    {
        comList = serv.sortC(sortBy, ascOrDesc);
        displayAllComputers();
    }
    disableButtons();
}

void MainWindow::on_addButton_clicked() //start the add dialog.
{
    addform add;
    add.setModal(true);
    add.exec();

    on_resetButton_clicked();
}

void MainWindow::on_viewImage_clicked() //prepare and start the view image dialog.
{
    viewImage viewImg;
    viewImg.setModal(true);

    QString idString = ui->listEntries->item(ui->listEntries->currentRow(),0)->text();
    int id = idString.toInt();
    viewImg.setImage(id, currentCategory);
    viewImg.exec();
    disableButtons();
}
