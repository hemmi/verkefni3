#include "baserepository.h"
#include <stdexcept>

bool baseRepository::createConnection()
{
    db = getDatabaseConnection();

    if(!db.isOpen())
    {
       throw std::runtime_error("Failed to open database");
    }
    return true;
}

QSqlDatabase baseRepository::getDatabaseConnection()
{
    QString connectionName = "ScienceConnection";

    QSqlDatabase database;

    if(QSqlDatabase::contains(connectionName))
    {
        database = QSqlDatabase::database(connectionName);
    }
    else
    {
        database = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        database.setDatabaseName("computer_science.sqlite");

        database.open();
    }

    return database;
}
