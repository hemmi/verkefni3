#ifndef INDIVIDUALREPOSITORY_H
#define INDIVIDUALREPOSITORY_H

#include "baserepository.h"
#include <iostream>
#include <individual.h>
#include "sciID.h"

class individualRepository : public baseRepository{
public:
    individualRepository();
    ~individualRepository();

    void addSci(individual ind);
    std::list<individual> getSlist();
    std::list<individual> sortS(string choice, string ascOrDesc);
    std::list<individual> findSci(string searchstr);
    int getRowIDSci();
    individual getConnectedScientist(int id);
    std::list<sciID> scientistsID();
    void connectSci(int rowID, int connectedID);
    std::list<individual> ConnectedScientists(int id);
    void remove(int id);
private:
    list<individual> slist;
    list<sciID> IDlist;
};

#endif // INDIVIDUALREPOSITORY_H
