#include "viewimage.h"
#include "ui_viewimage.h"
#include <QPixmap>

viewImage::viewImage(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::viewImage)
{
    ui->setupUi(this);
}

viewImage::~viewImage()
{
    delete ui;
}
void viewImage::setImage(int id, string currentCategory) //prepare the image pertaining to the entry selected by the user.
{
    if(currentCategory=="Computers")
    {
        Computer selectedCom = serv.getConnectedComputer(id);
        ui->labelName->setText(QString::fromStdString(selectedCom.getName()));

        QPixmap pixmap(QString::fromStdString(selectedCom.getImagePath()));

        int imageLabelWidth = ui->labelImage->width();
        QPixmap scaledPixMap = pixmap.scaledToWidth(imageLabelWidth);

        ui->labelImage->setPixmap(scaledPixMap);
    }
    if(currentCategory=="Scientists")
    {
        individual selectedInd = serv.getConnectedScientist(id);
        ui->labelName->setText(QString::fromStdString(selectedInd.getName()));

        QPixmap pixmap(QString::fromStdString(selectedInd.getImagePath()));

        int imageLabelWidth = ui->labelImage->width();
        QPixmap scaledPixMap = pixmap.scaledToWidth(imageLabelWidth);

        ui->labelImage->setPixmap(scaledPixMap);
    }
}
