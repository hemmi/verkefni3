#include "addform.h"
#include "ui_addform.h"
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>

addform::addform(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addform)
{
    ui->setupUi(this);
}

addform::~addform()
{
    delete ui;
}
void addform::on_add_sci_clicked() //add the input data to and instance of the computer class and send it onwards to the service layer.
{
    string namesci = ui->name_sci->text().toStdString();
    string yearbirth = ui->birth_sci->text().toStdString();
    string yeardeath = ui->death_sci->text().toStdString();
    string sexsci = ui->sex_sci->text().toStdString();
    string imagePath = ui->sciPathToImage->text().toStdString();

    if(validinput(yearbirth,yeardeath,sexsci))
    {

        serv.addSci(namesci, yearbirth, yeardeath, sexsci, imagePath);
        close();

        QMessageBox msgBox;//ask the user if he would like to connect the scientist to computers.
        msgBox.setText("Connect Scientist?");
        msgBox.setInformativeText("Connect this scientist to computers?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);
        int ret = msgBox.exec();

        switch (ret)
        {
            case QMessageBox::Yes:
                // call function to connect
                connection.setModal(true);
                connection.connectScientist();
                connection.exec();
                break;
            case QMessageBox::No:
                // Do nothing.
                break;
            default:
                // Do nothing.
                break;
         }
    }
}

bool addform::validinput(string yearb, string yeard, string sex) //validate the user input and display error messages if invalid.
{
    ui->error_sci->setText("");

    bool valid = true;

    if(ui->name_sci->text().isEmpty())
    {
        ui->error_sci->setText("Please enter a name!");
        valid = false;
    }

    else if(ui->birth_sci->text().isEmpty() || !(val.validateBirth(yearb)) )
    {
        ui->error_sci->setText("Error, invalid year of birth!");
        valid = false;
    }

    else if(!ui->death_sci->text().isEmpty() && !(val.validateDeath(yeard, yearb)) )
    {
        ui->error_sci->setText("Error, invalid year of death!");
        valid = false;
    }

    else if(ui->sex_sci->text().isEmpty() || !(val.validateSex(sex)) )
    {
        ui->error_sci->setText("Error, invalid sex!");
        valid = false;
    }

    return valid;
}

void addform::on_add_com_clicked() //add the input data to and instance of the computer class and send it onwards to the service layer.
{
    string namecom = ui->name_com->text().toStdString();
    string typecom = ui->type_com->text().toStdString();
    string wbuilt = ui->wbuilt_com->text().toStdString();
    string ybuilt = ui->ybuilt_com->text().toStdString();
    string imagePath = ui->comPathToImage->text().toStdString();

    if(validinputcom(namecom,typecom,wbuilt,ybuilt))
    {
        serv.addCom(namecom, typecom, wbuilt, ybuilt, imagePath);
        close();

        QMessageBox msgBox; //ask the user if he would like to connect the computer to scientists.
        msgBox.setText("Connect Computer?");
        msgBox.setInformativeText("Connect this computer to scientists?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);
        int ret = msgBox.exec();

        switch (ret)
        {
            case QMessageBox::Yes:
                // call function to connect
                connection.setModal(true);
                connection.connectComputer();
                connection.exec();
                break;
            case QMessageBox::No:
                // Do nothing.
                break;
            default:
                // Do nothing.
                break;
         }
    }
}

bool addform::validinputcom(string name, string type, string wbuilt,string ybuilt) //validate user input.
{
    ui->error_comp->setText("");

    bool valid = true;
    int intyear = atoi(ybuilt.c_str());

    if(name.empty())
    {
        ui->error_comp->setText("Error, invalid name!");
        valid = false;
    }

    else if(type.empty())
    {
        ui->error_comp->setText("Error, invalid type!");
        valid = false;
    }

    else if(wbuilt.empty() || !val.validateYesOrNo(wbuilt) )
    {
        ui->error_comp->setText("Error, invalid yes/no!");
        valid = false;
    }

    else if(!ybuilt.empty() && !val.validateYearbuilt(intyear))
    {
        ui->error_comp->setText("Error, invalid year of build!");
        valid = false;
    }
    else if(!ybuilt.empty() && wbuilt=="no" )
    {
        ui->error_comp->setText("Computer was not built.\nYear of Build must be empty!");
        valid = false;
    }
    else if(ybuilt.empty() && wbuilt=="yes" )
    {
        ui->error_comp->setText("Please enter year of build!");
        valid = false;
    }
    return valid;
}

void addform::on_comBrowseImage_clicked() //let the user select an image for the computer he is adding.
{
    QString imagePath = QFileDialog::getOpenFileName(
                this,
                "Browse for Image",
                "",
                "Image files (*.png *.jpg *.jpeg)"
    );
    ui->comPathToImage->setText(imagePath);
}

void addform::on_SciBrowseImage_clicked() //let the user select an image for the scientist he is adding.
{
    QString imagePath = QFileDialog::getOpenFileName(
                this,
                "Browse for Image",
                "",
                "Image files (*.png *.jpg *.jpeg)"
    );
    ui->sciPathToImage->setText(imagePath);
}
