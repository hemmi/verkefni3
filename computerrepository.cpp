#include "computerrepository.h"

computerRepository::computerRepository()
{
    createConnection();
}

computerRepository::~computerRepository()
{
}
void computerRepository::addCom(Computer com)
{
    string name = com.getName();
    string type = com.getType();
    string built = com.getBuilt();
    string yearb = com.getYB();
    string imagePath = com.getImagePath();

    QSqlQuery query(db); //add the new entry to the database.
    query.prepare("INSERT INTO Computers (Name, CType, Built, YBuilt, ImagePath)"
                  "VALUES(:name, :type, :built, :ybuilt, :imagepath)");

    query.bindValue(":name", QString::fromStdString(name));
    query.bindValue(":type", QString::fromStdString(type));
    query.bindValue(":built", QString::fromStdString(built));
    query.bindValue(":ybuilt", QString::fromStdString(yearb));
    query.bindValue(":imagepath", QString::fromStdString(imagePath));

    query.exec();

}

std::list<Computer> computerRepository::getClist()
{
   return clist;
}
std::list<Computer> computerRepository::sortC(string choice, string ascOrDesc)
{
    clist.clear();

    Computer c;

    string cname;
    string ybuilt;
    string type;
    string built;
    int id;
    string imagePath;

    QSqlQuery query(db);

    query.prepare(QString("SELECT * FROM Computers WHERE Deleted=0 ORDER BY %1 %2").arg(QString::fromStdString(choice)).arg(QString::fromStdString(ascOrDesc))); //Using arguments in QString: Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite

    query.exec();

    while(query.next())
    {
        cname = query.value("Name").toString().toStdString();
        ybuilt = query.value("YBuilt").toString().toStdString();
        type = query.value("CType").toString().toStdString();
        built = query.value("Built").toString().toStdString();
        id = query.value("ID").toInt();
        imagePath = query.value("ImagePath").toString().toStdString();

        // Load the sorted records into memoroy

        c = Computer(cname, ybuilt, type, built, id, imagePath);
        clist.push_back(c);
    }

     return clist;
}
std::list<Computer> computerRepository::find(string searchstr)
{
    clist.clear();

    Computer c;

    string cname;
    string ybuilt;
    string type;
    string built;
    int id;
    string imagePath;

    QSqlQuery query(db);

    query.prepare(QString("SELECT Name, CType, Built, YBuilt, ID, ImagePath FROM Computers WHERE Name || YBuilt ||CType || Built LIKE '%%1%' AND Deleted=0 ORDER BY ID").arg(QString::fromStdString(searchstr))); //Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite

    query.exec();

    while(query.next())
    {
       cname = query.value("Name").toString().toStdString();
       ybuilt = query.value("YBuilt").toString().toStdString();
       type = query.value("CType").toString().toStdString();
       built = query.value("Built").toString().toStdString();
       id = query.value("ID").toInt();
       imagePath = query.value("ImagePath").toString().toStdString();

       // Load all records matching the search string into memoroy

       c = Computer(cname, ybuilt, type, built, id, imagePath);
       clist.push_back(c);
    }
   return clist;
}
int computerRepository::getRowIDCom()
{

    QSqlQuery query(db);
    int rowid =0;

    query.prepare("SELECT MAX(ID) FROM Computers WHERE Deleted=0");
    query.exec();

    while(query.next())
    {
        rowid = query.value("MAX(ID)").toInt();
    }

    return rowid;
}
Computer computerRepository::getConnectedComputer(int id)
{
    Computer c;

    string cname;
    string ybuilt;
    string type;
    string built;
    int newid;
    string imagePath;

    QSqlQuery query(db);

    query.prepare(QString("SELECT * FROM Computers WHERE ID is %1 AND Deleted=0").arg(QString::number(id))); //Using arguments in QString: Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite
    query.exec();

    while(query.next())
    {
        cname = query.value("Name").toString().toStdString();
        ybuilt = query.value("YBuilt").toString().toStdString();
        type = query.value("CType").toString().toStdString();
        built = query.value("Built").toString().toStdString();
        newid = query.value("ID").toInt();
        imagePath = query.value("ImagePath").toString().toStdString();

        c = Computer(cname, ybuilt, type, built, newid, imagePath);
    }
    return c;
}
void computerRepository::connectCom(int rowID, int idToConnect)
{

        QSqlQuery query(db); //add the new entry to the database.

        query.prepare("INSERT INTO Connection (C_Id, S_Id)"
                      "VALUES(:cid, :sid)");
        query.bindValue(":cid", QString::number(rowID));
        query.bindValue(":sid", QString::number(idToConnect));
        query.exec();

}
std::list<Computer> computerRepository::ConnectedComputers(int id)
{
    Computer c;
    vector<int> tempList;
    clist.clear();

    int c_id;



    QSqlQuery query(db);

    query.prepare(QString("SELECT C_Id FROM Connection WHERE S_ID is %1").arg(QString::number(id))); //Using arguments in QString: Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite
    query.exec();

    while(query.next())
    {
        c_id = query.value("C_Id").toInt();
        tempList.push_back(c_id);
    }

    for(unsigned int i = 0; i < tempList.size(); i++)
    {
        c =  getConnectedComputer(tempList[i]);

        clist.push_back(c);
    }
    return clist;
}
std::list<sciID> computerRepository::computersID()
{
    IDlist.clear();

    sciID i;
    string id, name;

    QSqlQuery query(db);

    query.prepare(QString("SELECT ID, Name FROM Computers WHERE Deleted=0 ORDER BY ID"));

    query.exec();

    while(query.next())
    {
        id = query.value("ID").toString().toStdString();
        name = query.value("Name").toString().toStdString();

        i = sciID(id,name);
        IDlist.push_back(i);
    }

    return IDlist;
}

void computerRepository::remove(int id)
{
    QSqlQuery query(db);

    query.prepare(QString("UPDATE Computers SET Deleted=1 WHERE ID is %1").arg(QString::number(id))); //Using arguments in QString: Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite
    query.exec();
}
