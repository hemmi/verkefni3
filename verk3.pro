#-------------------------------------------------
#
# Project created by QtCreator 2014-12-13T15:04:54
#
#-------------------------------------------------

QT       += core sql gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = verk3
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    individual.cpp \
    sciID.cpp \
    validate.cpp \
    computer.cpp \
    service.cpp \
    addform.cpp \
    baserepository.cpp \
    computerrepository.cpp \
    individualrepository.cpp \
    viewconnections.cpp \
    createconnections.cpp \
    viewimage.cpp

HEADERS  += mainwindow.h \
    service.h \
    validate.h \
    sciID.h \
    computer.h \
    individual.h \
    addform.h \
    baserepository.h \
    computerrepository.h \
    individualrepository.h \
    viewconnections.h \
    createconnections.h \
    viewimage.h

FORMS    += mainwindow.ui \
    viewconnections.ui \
    createconnections.ui \
    viewimage.ui

    addform.h

FORMS    += mainwindow.ui \
    addform.ui

RESOURCES += \
    resource_files.qrc

