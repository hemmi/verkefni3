#ifndef REMOVEFORM_H
#define REMOVEFORM_H

#include <QWidget>

namespace Ui {
class removeform;
}

class removeform : public QWidget
{
    Q_OBJECT

public:
    explicit removeform(QWidget *parent = 0);
    ~removeform();

private slots:
    void on_listView_2_activated(const QModelIndex &index);

private:
    Ui::removeform *ui;
};

#endif // REMOVEFORM_H
