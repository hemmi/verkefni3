#include "repository.h"

repository::repository() //add the database.
{
    db = QSqlDatabase::database();
    db = QSqlDatabase::addDatabase("QSQLITE");
    QString dbName = "computer_science.sqlite";
    db.setDatabaseName(dbName);
    db.open();
}

repository::~repository(){
    db.close();
}

void repository::addCom(Computer com)
{
    string name = com.getName();
    string type = com.getType();
    string built = com.getBuilt();
    string yearb = com.getYB();

    QSqlQuery query; //add the new entry to the database.

    query.prepare("INSERT INTO Computers (Name, CType, Built, YBuilt)"
                  "VALUES(:name, :type, :built, :ybuilt)");

    query.bindValue(":name", QString::fromStdString(name));
    query.bindValue(":type", QString::fromStdString(type));
    query.bindValue(":built", QString::fromStdString(built));
    query.bindValue(":ybuilt", QString::fromStdString(yearb));

    query.exec();

}

std::list<Computer> repository::getClist()
{
   return clist;
}
std::list<Computer> repository::sortC(string choice, string ascOrDesc)
{
    clist.clear();

    Computer c;

    string cname;
    string ybuilt;
    string type;
    string built;
    int id;

    QSqlQuery query;

    query.prepare(QString("SELECT * FROM Computers WHERE Deleted=0 ORDER BY %1 %2").arg(QString::fromStdString(choice)).arg(QString::fromStdString(ascOrDesc))); //Using arguments in QString: Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite

    query.exec();

    while(query.next())
    {
        cname = query.value("Name").toString().toStdString();
        ybuilt = query.value("YBuilt").toString().toStdString();
        type = query.value("CType").toString().toStdString();
        built = query.value("Built").toString().toStdString();
        id = query.value("ID").toInt();

        // Load the sorted records into memoroy

        c = Computer(cname, ybuilt, type, built, id);
        clist.push_back(c);
    }

     return clist;
}
std::list<Computer> repository::find(string searchstr)
{
    clist.clear();

    Computer c;

    string cname;
    string ybuilt;
    string type;
    string built;
    int id;

    QSqlQuery query;

    query.prepare(QString("SELECT Name, CType, Built, YBuilt, ID FROM Computers WHERE Name || YBuilt ||CType || Built LIKE '%%1%' AND Deleted=0 ORDER BY ID").arg(QString::fromStdString(searchstr))); //Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite

    query.exec();

    while(query.next())
    {
       cname = query.value("Name").toString().toStdString();
       ybuilt = query.value("YBuilt").toString().toStdString();
       type = query.value("CType").toString().toStdString();
       built = query.value("Built").toString().toStdString();
       id = query.value("ID").toInt();

       // Load all records matching the search string into memoroy

       c = Computer(cname, ybuilt, type, built, id);
       clist.push_back(c);
    }
   return clist;
}
int repository::getRowIDCom()
{

    QSqlQuery query;
    int rowid =0;

    query.prepare("SELECT MAX(ID) FROM Computers AND Deleted=0");
    query.exec();

    while(query.next())
    {
        rowid = query.value("MAX(ID)").toInt();
    }

    return rowid;
}
Computer repository::getConnectedComputer(int id)
{
    Computer c;

    string cname;
    string ybuilt;
    string type;
    string built;
    int newid;

    QSqlQuery query;

    query.prepare(QString("SELECT * FROM Computers WHERE ID is %1 AND Deleted=0").arg(QString::number(id))); //Using arguments in QString: Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite
    query.exec();

    while(query.next())
    {
        cname = query.value("Name").toString().toStdString();
        ybuilt = query.value("YBuilt").toString().toStdString();
        type = query.value("CType").toString().toStdString();
        built = query.value("Built").toString().toStdString();
        newid = query.value("ID").toInt();

        c = Computer(cname, ybuilt, type, built, newid);
    }
    return c;
}
std::list<sciID> repository::computersID()
{
    IDlist.clear();

    sciID i;
    string id, name;

    QSqlQuery query;

    query.prepare(QString("SELECT ID, Name FROM Computers WHERE Deleted=0 ORDER BY ID"));

    query.exec();

    while(query.next())
    {
        id = query.value("ID").toString().toStdString();
        name = query.value("Name").toString().toStdString();

        i = sciID(id,name);
        IDlist.push_back(i);
    }

    return IDlist;
}
void repository::connectCom(int id, vector<int> idlist)
{


    for(unsigned int i = 0; i < idlist.size(); i++)
    {
        QSqlQuery query; //add the new entry to the database.

        query.prepare("INSERT INTO Connection (C_Id, S_Id)"
                      "VALUES(:cid, :sid)");
        query.bindValue(":cid", QString::number(id));
        query.bindValue(":sid", QString::number(idlist[i]));
        query.exec();
    }

}
std::list<Computer> repository::ConnectedComputers(int id)
{
    Computer c;
    vector<int> tempList;
    clist.clear();

    int c_id;



    QSqlQuery query;

    query.prepare(QString("SELECT C_Id FROM Connection WHERE S_ID is %1").arg(QString::number(id))); //Using arguments in QString: Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite
    query.exec();

    while(query.next())
    {
        c_id = query.value("C_Id").toInt();
        tempList.push_back(c_id);
    }

    for(unsigned int i = 0; i < tempList.size(); i++)
    {
        c =  getConnectedComputer(tempList[i]);

        clist.push_back(c);
    }
    return clist;
}

void repository::addSci(individual ind)
{
    string name = ind.getName();
    string yearb = ind.getYB();
    string yeard = ind.getYD();
    string sex = ind.getSex();

    QSqlQuery query; //add the new entry to the database.

    query.prepare("INSERT INTO Scientists (Name, YoB, YoD, Sex)"
                  "VALUES(:name, :yearb, :yeard, :sex)");

    query.bindValue(":name", QString::fromStdString(name));
    query.bindValue(":yearb", QString::fromStdString(yearb));
    query.bindValue(":yeard", QString::fromStdString(yeard));
    query.bindValue(":sex", QString::fromStdString(sex));

    query.exec();

}
std::list<individual> repository::getSlist()
{
   return slist;
}
std::list<individual> repository::sortS(string choice, string ascOrDesc)
{
    slist.clear();
    individual ind;
    string name;
    string yearB, yearD;
    string sex;
    int id;

    //Retrieve scientists

    QSqlQuery query;

    query.prepare(QString("SELECT Name, YoB, YoD, Sex, ID FROM Scientists WHERE Deleted=0 ORDER BY %1 %2").arg(QString::fromStdString(choice)).arg(QString::fromStdString(ascOrDesc))); //Using agruments in QString Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite

    query.exec();

    while(query.next())
    {
        name = query.value("Name").toString().toStdString();
        yearB = query.value("YoB").toString().toStdString();
        yearD = query.value("YoD").toString().toStdString();
        sex = query.value("Sex").toString().toStdString();
        id = query.value("ID").toInt();

        // Load all records into memoroy

        ind = individual(name, yearB, yearD, sex, id);
        slist.push_back(ind);
    }
    return slist;
}

std::list<individual> repository::findSci(string searchstr)
{
    slist.clear();

    individual i;
    string name;
    string yearB, yearD;
    string sex;
    int id;

    QSqlQuery query;

    query.prepare(QString("SELECT Name, YoB, YoD, Sex ,ID FROM Scientists WHERE Name || YoB ||YoD || Sex LIKE '%%1%' AND Deleted=0 ORDER BY ID").arg(QString::fromStdString(searchstr))); //Using aguments in QString Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite

    query.exec();

    while(query.next())
    {
       name = query.value("Name").toString().toStdString();
       yearB = query.value("YoB").toString().toStdString();
       yearD = query.value("YoD").toString().toStdString();
       sex = query.value("Sex").toString().toStdString();
       id = query.value("ID").toInt();

       // Load all records matching the search string into memoroy

       i = individual(name, yearB, yearD, sex, id);
       slist.push_back(i);
    }
    return slist;
}

int repository::getRowIDSci()
{

    QSqlQuery query;
    int rowid=0;

    query.prepare("SELECT MAX(ID) FROM Scientists");
    query.exec();

    while(query.next())
    {
        rowid = query.value("MAX(ID)").toInt();
    }

    return rowid;
}
individual repository::getConnectedScientist(int id)
{
    individual i;

    string name;
    string yearB, yearD;
    string sex;
    int newid;

    QSqlQuery query;

    query.prepare(QString("SELECT Name, YoB, YoD, Sex, ID FROM Scientists WHERE ID is %1").arg(QString::number(id))); //Using aguments in QString Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite
    query.exec();

    while(query.next())
    {
       name = query.value("Name").toString().toStdString();
       yearB = query.value("YoB").toString().toStdString();
       yearD = query.value("YoD").toString().toStdString();
       sex = query.value("Sex").toString().toStdString();
       newid = query.value("ID").toInt();

       i = individual(name, yearB, yearD, sex, newid);
    }
    return i;
}

std::list<sciID> repository::scientistsID()
{
    IDlist.clear();

    sciID i;

    string id,name;

    QSqlQuery query;

    query.prepare(QString("SELECT ID, Name FROM Scientists ORDER BY ID"));

    query.exec();

    while(query.next())
    {
        id = query.value("ID").toString().toStdString();
        name = query.value("Name").toString().toStdString();

        i = sciID(id,name);
        IDlist.push_back(i);
    }

    return IDlist;
}

void repository::connectSci(int id, vector<int> idlist)
{

    for(unsigned int i=0;i<idlist.size();i++)
    {
        QSqlQuery query; //add the new entry to the database.

        query.prepare("INSERT INTO Connection (C_Id, S_Id)"
                      "VALUES(:cid, :sid)");
        query.bindValue(":cid", QString::number(idlist[i]));
        query.bindValue(":sid", QString::number(id));
        query.exec();
    }

}

std::list<individual> repository::ConnectedScientists(int id)
{
    individual ind;
    vector<int> tempList;
    slist.clear();

    int s_id;

    QSqlQuery query;

    query.prepare(QString("SELECT S_Id FROM Connection WHERE c_Id is %1").arg(QString::number(id))); //Using arguments in QString: Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite
    query.exec();

    while(query.next())
    {
        s_id = query.value("S_Id").toInt();
        tempList.push_back(s_id);
    }

    for(unsigned int i = 0; i < tempList.size(); i++)
    {
        ind =  getConnectedScientist(tempList[i]);
       slist.push_back(ind);
    }
    return slist;
}
void repository::remove(int id, string currentCategory){

    QSqlQuery query;

    query.prepare(QString("UPDATE %2 SET Deleted=1 WHERE ID is %1").arg(QString::number(id)).arg(QString::fromUtf8(currentCategory.c_str()))); //Using arguments in QString: Source Amartel, http://stackoverflow.com/questions/15902859/qsqlquery-with-prepare-and-bindvalue-for-column-name-sqlite
    query.exec();
}
