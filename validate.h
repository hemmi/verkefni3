#ifndef VALIDATE_H
#define VALIDATE_H
#include <string>
#include <vector>
#include <list>
#include "computer.h"
#include "individual.h"
#include "sciID.h"
#include <sstream>
#include <stdlib.h>

using namespace std;

class validate //validates input from the user.
{
public:

    validate();

    void validateString(string& name);
    bool validateDeath(string yearDeath, string yearBirth);
    bool validateBirth(string year);
    bool validateSex(string &sex);
    bool validateYesOrNo(string input);

    bool validateID(std::list<sciID> list, int id);
    bool validateYearbuilt(int year);
};

#endif // VALIDATE_H
