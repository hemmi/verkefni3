#include "createconnections.h"
#include "ui_createconnections.h"
#include <QDebug>

createConnections::createConnections(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::createConnections)
{
    ui->setupUi(this);
}

createConnections::~createConnections()
{
    delete ui;
}
void createConnections::connectComputer()//connect a computer to scientists.
{

    currentCategory = "Computers";
    sID.clear();
    sID = serv.getSciIDlist();
    populatePossibleConnections();
}
void createConnections::connectScientist()//connect a scientist to computers.
{
    currentCategory = "Scientists";
    sID.clear();
    sID = serv.getComIDlist();
    populatePossibleConnections();
}
void createConnections::populatePossibleConnections()//populate a list of possible connections for the new entry.
{
    while (ui->possibleConnections->rowCount() > 0)
    {
        ui->possibleConnections->removeRow(0);
    }

    ui->possibleConnections->verticalHeader()->setVisible(false);
    ui->possibleConnections->setColumnWidth(0,50);
    ui->possibleConnections->setColumnWidth(1,225);

    int i = 0;

    for(std::list<sciID>::iterator iter = sID.begin(); iter != sID.end(); iter++, i++)
    {
        QTableWidgetItem *item0 = new QTableWidgetItem(QString::fromStdString(iter->getID()));
        QTableWidgetItem *item1 = new QTableWidgetItem(QString::fromStdString(iter->getName()));

        ui->possibleConnections->insertRow(i);
        ui->possibleConnections->setItem(i,0,item0);
        ui->possibleConnections->setItem(i,1,item1);
    }
}

void createConnections::on_connectButton_clicked()
{
    if(currentCategory=="Computers")
    {
        rowID = serv.getRowIDCom(); //get the ID of the computer being added.

        QString idString = ui->possibleConnections->item(ui->possibleConnections->currentRow(),0)->text();
        int id = idString.toInt(); //the id of the scientist the computer should be connected to.

        serv.connectCom(rowID,id);
        ui->possibleConnections->removeRow(ui->possibleConnections->currentRow()); //remove the row containing the scientist so it can not be connected again.
        ui->connectButton->setEnabled(false); //disable the button again so user has to select another row.
        ui->connectionAdded->setText("Connection Added!");
    }
    if(currentCategory=="Scientists")
    {
        rowID = serv.getRowIDSci(); //get the ID of the scientist being added.

        QString idString = ui->possibleConnections->item(ui->possibleConnections->currentRow(),0)->text();
        int id = idString.toInt(); //the id of the computer the scientist should be connected to.

        serv.connectSci(rowID,id);
        ui->possibleConnections->removeRow(ui->possibleConnections->currentRow()); //remove the row containing the scientist so it can not be connected again.
        ui->connectButton->setEnabled(false); //disable the button again so user has to select another row.
        ui->connectionAdded->setText("Connection Added!");
    }
}

void createConnections::on_possibleConnections_itemSelectionChanged()
{
    ui->connectButton->setEnabled(true); //enable the connect button if an item in the table is selected.
    ui->connectionAdded->clear();
}

void createConnections::on_pushButton_3_clicked()
{
    close();
}
