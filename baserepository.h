#ifndef BASEREPOSITORY_H
#define BASEREPOSITORY_H

#include <QtSql>

class baseRepository{
    protected:
        QSqlDatabase db;

        bool createConnection();
        QSqlDatabase getDatabaseConnection();
};

#endif // BASEREPOSITORY_H
