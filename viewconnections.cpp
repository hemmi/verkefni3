#include "viewconnections.h"
#include "ui_viewconnections.h"
#include <QDebug>

viewConnections::viewConnections(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::viewConnections)
{
    ui->setupUi(this);
}

viewConnections::~viewConnections()
{
    delete ui;
}
void viewConnections::getIdFromMain(int id, string currentCategory) //retrieve an instance of either scientist or computer using the id of the seleced item in the table.
{
    forwardedID = id;
    forwardedCategory = currentCategory;

    if(forwardedCategory=="Scientists")
    {
        selectedSci = serv.getConnectedScientist(id);
        connectedComs = serv.ConnectedComputers(id);
        populateTableSelSci(selectedSci, connectedComs);
    }
    if(forwardedCategory=="Computers")
    {
        selectedCom = serv.getConnectedComputer(id);
        connectedScis = serv.ConnectedScientists(id);
        populateTableSelCom(selectedCom, connectedScis);
    }
}
void viewConnections::populateTableSelCom(Computer selectedCom, std::list<individual> connectedScis)//populate the two tables selected entry and connections to that entry.
{
    ui->selectedLabel->setText("You have selected the following computer:");
    ui->connectedLabel->setText("It is connected to the following scientists:");

    ui->selectedTable->verticalHeader()->setVisible(false);
    ui->connectedTable->verticalHeader()->setVisible(false);

    //display the selected computer

    ui->selectedTable->setHorizontalHeaderItem(0, new QTableWidgetItem("ID"));
    ui->selectedTable->setHorizontalHeaderItem(1, new QTableWidgetItem("Name"));
    ui->selectedTable->setHorizontalHeaderItem(2, new QTableWidgetItem("Type"));
    ui->selectedTable->setHorizontalHeaderItem(3, new QTableWidgetItem("Built?"));
    ui->selectedTable->setHorizontalHeaderItem(4, new QTableWidgetItem("Year Built"));

    ui->selectedTable->clearContents();

    while (ui->selectedTable->rowCount() > 0)
    {
        ui->selectedTable->removeRow(0);
    }

    ui->selectedTable->setColumnWidth(0,50);
    ui->selectedTable->setColumnWidth(1,225);
    ui->selectedTable->setColumnWidth(2,210);
    ui->selectedTable->setColumnWidth(3,60);
    ui->selectedTable->setColumnWidth(4,100);

        QTableWidgetItem *item0 = new QTableWidgetItem(QString::number(selectedCom.getID()));
        QTableWidgetItem *item1 = new QTableWidgetItem(QString::fromStdString(selectedCom.getName()));
        QTableWidgetItem *item2 = new QTableWidgetItem(QString::fromStdString(selectedCom.getType()));
        QTableWidgetItem *item3 = new QTableWidgetItem(QString::fromStdString(selectedCom.getBuilt()));
        QTableWidgetItem *item4 = new QTableWidgetItem(QString::fromStdString(selectedCom.getYB()));

        ui->selectedTable->insertRow(0);
        ui->selectedTable->setItem(0,0,item0);
        ui->selectedTable->setItem(0,1,item1);
        ui->selectedTable->setItem(0,2,item2);
        ui->selectedTable->setItem(0,3,item3);
        ui->selectedTable->setItem(0,4,item4);

        //Display the Scientists the computer is connected to.

        int i = 0;
        ui->connectedTable->horizontalHeaderItem(0)->setText("ID");
        ui->connectedTable->horizontalHeaderItem(1)->setText("Name");
        ui->connectedTable->horizontalHeaderItem(2)->setText("Year of Birth");
        ui->connectedTable->horizontalHeaderItem(3)->setText("Year of Death");
        ui->connectedTable->horizontalHeaderItem(4)->setText("Sex");

        ui->connectedTable->clearContents();

        while (ui->connectedTable->rowCount() > 0)
        {
            ui->connectedTable->removeRow(0);
        }

        ui->connectedTable->setColumnWidth(0,40);
        ui->connectedTable->setColumnWidth(1,260);
        ui->connectedTable->setColumnWidth(2,130);
        ui->connectedTable->setColumnWidth(3,130);
        ui->connectedTable->setColumnWidth(4,60);

        for(std::list<individual>::iterator iter = connectedScis.begin(); iter != connectedScis.end(); iter++, i++)
        {
            QTableWidgetItem *item0 = new QTableWidgetItem(QString::number(iter->getID()));
            QTableWidgetItem *item1 = new QTableWidgetItem(QString::fromStdString(iter->getName()));
            QTableWidgetItem *item2 = new QTableWidgetItem(QString::fromStdString(iter->getYB()));
            QTableWidgetItem *item3 = new QTableWidgetItem(QString::fromStdString(iter->getYD()));
            QTableWidgetItem *item4 = new QTableWidgetItem(QString::fromStdString(iter->getSex()));

            ui->connectedTable->insertRow(i);
            ui->connectedTable->setItem(i,0,item0);
            ui->connectedTable->setItem(i,1,item1);
            ui->connectedTable->setItem(i,2,item2);
            ui->connectedTable->setItem(i,3,item3);
            ui->connectedTable->setItem(i,4,item4);
        }
}
void viewConnections::populateTableSelSci(individual selectedSci, std::list<Computer> connectedComs) //populate the two tables selected entry and connections to that entry.
{

    ui->selectedLabel->setText("You have selected the following scientist:");
    ui->connectedLabel->setText("They are connected to the following computers:");

    ui->selectedTable->verticalHeader()->setVisible(false);
    ui->connectedTable->verticalHeader()->setVisible(false);

    //display the selected scientsist

    ui->selectedTable->horizontalHeaderItem(0)->setText("ID");
    ui->selectedTable->horizontalHeaderItem(1)->setText("Name");
    ui->selectedTable->horizontalHeaderItem(2)->setText("Year of Birth");
    ui->selectedTable->horizontalHeaderItem(3)->setText("Year of Death");
    ui->selectedTable->horizontalHeaderItem(4)->setText("Sex");

    ui->selectedTable->clearContents();

    while (ui->selectedTable->rowCount() > 0)
    {
        ui->selectedTable->removeRow(0);
    }

    ui->selectedTable->setColumnWidth(0,40);
    ui->selectedTable->setColumnWidth(1,260);
    ui->selectedTable->setColumnWidth(2,130);
    ui->selectedTable->setColumnWidth(3,130);
    ui->selectedTable->setColumnWidth(4,60);

    QTableWidgetItem *item0 = new QTableWidgetItem(QString::number(selectedSci.getID()));
    QTableWidgetItem *item1 = new QTableWidgetItem(QString::fromStdString(selectedSci.getName()));
    QTableWidgetItem *item2 = new QTableWidgetItem(QString::fromStdString(selectedSci.getYB()));
    QTableWidgetItem *item3 = new QTableWidgetItem(QString::fromStdString(selectedSci.getYD()));
    QTableWidgetItem *item4 = new QTableWidgetItem(QString::fromStdString(selectedSci.getSex()));

    ui->selectedTable->insertRow(0);
    ui->selectedTable->setItem(0,0,item0);
    ui->selectedTable->setItem(0,1,item1);
    ui->selectedTable->setItem(0,2,item2);
    ui->selectedTable->setItem(0,3,item3);
    ui->selectedTable->setItem(0,4,item4);

    //Display the Computers the scientist is connected to.

    int i = 0;
    ui->connectedTable->setHorizontalHeaderItem(0, new QTableWidgetItem("ID"));
    ui->connectedTable->setHorizontalHeaderItem(1, new QTableWidgetItem("Name"));
    ui->connectedTable->setHorizontalHeaderItem(2, new QTableWidgetItem("Type"));
    ui->connectedTable->setHorizontalHeaderItem(3, new QTableWidgetItem("Built?"));
    ui->connectedTable->setHorizontalHeaderItem(4, new QTableWidgetItem("Year Built"));

    ui->connectedTable->clearContents();

    while (ui->connectedTable->rowCount() > 0)
    {
        ui->connectedTable->removeRow(0);
    }

    ui->connectedTable->setColumnWidth(0,50);
    ui->connectedTable->setColumnWidth(1,225);
    ui->connectedTable->setColumnWidth(2,210);
    ui->connectedTable->setColumnWidth(3,60);
    ui->connectedTable->setColumnWidth(4,100);

    for(std::list<Computer>::iterator iter = connectedComs.begin(); iter != connectedComs.end(); iter++, i++)
    {
        QTableWidgetItem *item0 = new QTableWidgetItem(QString::number(iter->getID()));
        QTableWidgetItem *item1 = new QTableWidgetItem(QString::fromStdString(iter->getName()));
        QTableWidgetItem *item2 = new QTableWidgetItem(QString::fromStdString(iter->getType()));
        QTableWidgetItem *item3 = new QTableWidgetItem(QString::fromStdString(iter->getBuilt()));
        QTableWidgetItem *item4 = new QTableWidgetItem(QString::fromStdString(iter->getYB()));

        ui->connectedTable->insertRow(i);
        ui->connectedTable->setItem(i,0,item0);
        ui->connectedTable->setItem(i,1,item1);
        ui->connectedTable->setItem(i,2,item2);
        ui->connectedTable->setItem(i,3,item3);
        ui->connectedTable->setItem(i,4,item4);
    }
}
