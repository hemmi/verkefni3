#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "service.h"
#include "validate.h"
#include "computer.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void getAllScientists();
    void displayAllScientists();
    void displayAllComputers();
    void getAllComputers();
    void disableButtons();
private slots:
    void on_categoryBox_activated(int index);
    void on_searchButton_clicked();
    void on_resetButton_clicked();
    void on_sortBox_activated(int index);
    void on_ascRadioButton_clicked();
    void on_descRadioButton_clicked();
    void on_listEntries_itemSelectionChanged();
    void on_connectionsButton_clicked();
    void on_search_input_cursorPositionChanged();
    void on_removeButton_clicked();
    void on_addButton_clicked();
    void on_viewImage_clicked();

private:
    Ui::MainWindow *ui;
    service serv;
    validate val;
    std::list<individual> sciList;
    std::list<Computer> comList;
    string currentCategory;
    string sortBy;
    string ascOrDesc;

  //  addform *addsome;
};

#endif // MAINWINDOW_H
