#ifndef CREATECONNECTIONS_H
#define CREATECONNECTIONS_H

#include <QDialog>
#include "service.h"
#include "computer.h"
#include "individual.h"
#include "sciID.h"

namespace Ui {
class createConnections;
}

class createConnections : public QDialog
{
    Q_OBJECT

public:
    explicit createConnections(QWidget *parent = 0);
    ~createConnections();

    void connectComputer();
    void connectScientist();
    void populatePossibleConnections();

private slots:
    void on_connectButton_clicked();
    void on_possibleConnections_itemSelectionChanged();
    void on_pushButton_3_clicked();

private:
    Ui::createConnections *ui;
    service serv;
    int rowID;
    std::list<sciID> sID;
    string currentCategory;
};

#endif // CREATECONNECTIONS_H
