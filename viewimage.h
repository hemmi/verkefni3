#ifndef VIEWIMAGE_H
#define VIEWIMAGE_H
#include "service.h"
#include <QDialog>

namespace Ui {
class viewImage;
}

class viewImage : public QDialog
{
    Q_OBJECT

public:
    explicit viewImage(QWidget *parent = 0);
    ~viewImage();

    void setImage(int id, string currentCategory);
private:
    Ui::viewImage *ui;
    service serv;
};

#endif // VIEWIMAGE_H
