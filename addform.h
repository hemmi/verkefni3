#ifndef ADDFORM_H
#define ADDFORM_H

#include <QDialog>
#include <validate.h>
#include <service.h>
#include <createconnections.h>

namespace Ui {
class addform;
}

class addform : public QDialog
{
    Q_OBJECT

public:
    explicit addform(QWidget *parent = 0);
    ~addform();

private slots:
    void on_add_sci_clicked();
    void on_add_com_clicked();
    void on_comBrowseImage_clicked();
    void on_SciBrowseImage_clicked();

private:
    bool validinput(string yearb, string yeard, string sex);
    bool validinputcom(string name, string type, string ifbuilt, string built);

    Ui::addform *ui;
    validate val;
    service serv;
    createConnections connection;
};

#endif // ADDFORM_H
