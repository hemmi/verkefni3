#include "addform2.h"
#include "ui_addform.h"

addform::addform(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::addform)
{
    ui->setupUi(this);
}

addform::~addform()
{
    delete ui;
}

void addform::on_add_sci_clicked()
{
    string namesci = ui->name_sci->text().toStdString();
    string yearbirth = ui->birth_sci->text().toStdString();
    string yeardeath = ui->death_sci->text().toStdString();
    string sexsci = ui->sex_sci->text().toStdString();

    if(validinput(yearbirth,yeardeath,sexsci)){
        serv.addSci(namesci,yearbirth,yeardeath,sexsci);
        close();
    }
}

bool addform::validinput(string yearb, string yeard, string sex){
    ui->error_sci->setText("");

    bool valid = true;

    if(ui->name_sci->text().isEmpty()){
        ui->error_sci->setText("Error, invalid name!");
        valid = false;
    }

    else if(ui->birth_sci->text().isEmpty() || !(val.validateBirth(yearb)) ){
        ui->error_sci->setText("Error, invalid birth!");
        valid = false;
    }

    else if(ui->death_sci->text().isEmpty() || !(val.validateDeath(yeard, yearb)) ){
        ui->error_sci->setText("Error, invalid death!");
        valid = false;
    }

    else if(ui->sex_sci->text().isEmpty() || !(val.validateSex(sex)) ){
        ui->error_sci->setText("Error, invalid sex!");
        valid = false;
    }

    return valid;
}

void addform::on_add_com_clicked()
{
    string namecom = ui->name_sci->text().toStdString();
    string typecom = ui->birth_sci->text().toStdString();
    string ifbuilt = ui->death_sci->text().toStdString();
    string built = ui->sex_sci->text().toStdString();

    if(validinputcom(ifbuilt,built)){
        serv.addCom(namecom,typecom,ifbuilt,built);
        close();
    }
}

bool addform::validinputcom(string ifbuilt,string built){
    ui->error_comp->setText("");

    bool valid = true;

    if(ui->name_com->text().isEmpty()){
        ui->error_comp->setText("Error, invalid name!");
        valid = false;
    }

    else if(ui->type_com->text().isEmpty()){
        ui->error_comp->setText("Error, invalid type!");
        valid = false;
    }

    else if(ui->wbuilt_com->text().isEmpty() || val.validateYesOrNo(ifbuilt) ){
        ui->error_comp->setText("Error, invalid yes/no!");
        valid = false;
    }

    else if(ui->ybuilt_com->text().isEmpty() || val.validateYearbuilt(built) ){
        ui->error_comp->setText("Error, invalid built!");
        valid = false;
    }

    return valid;
}
