#ifndef COMPUTERREPOSITORY_H
#define COMPUTERREPOSITORY_H

#include "baserepository.h"
#include "computer.h"
#include <iostream>
#include "sciID.h"

class computerRepository : public baseRepository
{
public:
    computerRepository();
    ~computerRepository();
    void addCom(Computer com);
    std::list<Computer> getClist();
    std::list<Computer> sortC(string choice, string ascOrDesc);
    std::list<Computer> find(string searchstr);
    int getRowIDCom();
    Computer getConnectedComputer(int id);
    std::list<sciID> computersID();
    void connectCom(int rowID, int idToConnect);
    std::list<Computer> ConnectedComputers(int id);
    void remove(int id);
private:
    list<Computer> clist;
    list<sciID> IDlist;
};
#endif // COMPUTERREPOSITORY_H
