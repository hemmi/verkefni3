#ifndef SCIID_H
#define SCIID_H

#include <string>
#include <iomanip>
#include <iostream>

using namespace std;
class sciID
{
public:
    sciID();
    sciID(const string &i, const string &n);
    friend ostream& operator<<(ostream& out, sciID s);
    string getID();
    string getName();
private:
    string id;
    string name;
};

#endif // SCIID_H
