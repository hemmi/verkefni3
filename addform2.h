#ifndef ADDFORM_H
#define ADDFORM_H

#include <QWidget>
#include <validate.h>
#include <service.h>

namespace Ui {
class addform;
}

class addform : public QWidget
{
    Q_OBJECT

public:
    explicit addform(QWidget *parent = 0);
    ~addform();

private slots:
    void on_add_sci_clicked();
    void on_add_com_clicked();
private:
    bool validinput(string yearb, string yeard, string sex);
    bool validinputcom(string ifbuilt, string built);

    Ui::addform *ui;
    validate val;
    service serv;
};

#endif // ADDFORM_H
