#include "service.h"
#include <qdebug.h>

service::service()
{
}
void service::remove(int id, string currentCategory)
{
    if(currentCategory=="Scientists")
    {
        indRepo.remove(id);
    }
    if(currentCategory=="Computers")
    {
        comRepo.remove(id);
    }
}
list<Computer> service::search(string searchterm)//search through the Computers.
{
    std::list<Computer> sr = comRepo.find(searchterm);
    return sr;
}

string service::assessOrder(string choice)
{
    string order;

    if (choice == "name")
    {
       order = "Name, CType, Built, YBuilt";
    }
    else if(choice == "year")
    {
        order = "YBuilt, Name, Ctype, Built";
    }
    else if(choice == "type")
    {
        order = "Ctype, Name, Built, YBuilt";
    }
    else if(choice == "built")
    {
        order = "Built, Name, Ctype, YBuilt";
    }
    return order;
}

std::list<Computer> service::sortC(string order, string ascOrDesc)
{
    std::list<Computer> sortedlist = comRepo.sortC(order, ascOrDesc);
    return sortedlist;
}
void service::addCom(const string& name, const string &type, const string &built, const string &ybuilt, string &imagePath)//add a new name.
{

    Computer c(name, ybuilt, type, built, 0,imagePath);
    comRepo.addCom(c);
}
list<Computer> service::listCom() //prepare the contents of the list for being printed on the user's screen.
{
    std::list<Computer> complist = comRepo.getClist();
    return complist;
}

int service::getRowIDCom()
{
    int id = comRepo.getRowIDCom();
    return id;
}
std::list<sciID> service::getComIDlist()
{
    std::list<sciID> sID = comRepo.computersID();
    return sID;
}
void service::connectCom(int rowID, int idToConnect)
{
    comRepo.connectCom(rowID, idToConnect);
}
Computer service::getConnectedComputer(int id)
{
    Computer com = comRepo.getConnectedComputer(id);
    return com;
}
std::list<Computer> service::ConnectedComputers(int id)
{
    std::list<Computer> listOfConCom = comRepo.ConnectedComputers(id);
    return listOfConCom;
}

list<individual> service::searchSci(string searchterm)//search through the Scientists.
{
    std::list<individual> sr = indRepo.findSci(searchterm);
    return sr;
}

string service::assessOrderS(string choice)
{
    string order;

    if (choice == "name")
    {
       order = "Name";
    }
    else if(choice == "birth")
    {
        order = "YoB, Name";
    }
    else if(choice == "death")
    {
        order = "YoD, Name";
    }
    else if(choice == "sex")
    {
        order = "Sex, Name";
    }
    return order;
}

std::list<individual> service::sortS(string order, string ascOrDesc)
{
    std::list<individual> sortedlist = indRepo.sortS(order, ascOrDesc);
    return sortedlist;
}
void service::addSci(const string& name, const string &yearb, const string &yeard, const string &sex, string imagePath)//add a new name.
{
    int dummyid=0;
    individual i(name, yearb, yeard, sex, dummyid, imagePath);
    indRepo.addSci(i);
}
list<individual> service::listSci() //prepare the contents of the list for being printed on the user's screen.
{
    std::list<individual> scilist = indRepo.getSlist();
    return scilist;
}

int service::getRowIDSci()
{
    int id = indRepo.getRowIDSci();
    return id;
}

std::list<sciID> service::getSciIDlist()
{
    std::list<sciID> sID = indRepo.scientistsID();
    return sID;
}

void service::connectSci(int rowID, int connectID)
{
    indRepo.connectSci(rowID, connectID);
}
individual service::getConnectedScientist(int id)
{
    individual ind = indRepo.getConnectedScientist(id);
    return ind;
}
std::list<individual> service::ConnectedScientists(int id)
{
    std::list<individual> listOfConCom = indRepo.ConnectedScientists(id);
    return listOfConCom;
}
